import flask
import numpy as np
import tensorflow as tf

import json
from PIL import Image


app = flask.Flask(__name__)

def load_model():
    global graph, model
    model = tf.keras.models.load_model('MNIST.h5')
    graph = tf.get_default_graph()

@app.route("/predict", methods = ["POST"])
def prediction():
    images = flask.request.files.getlist('images')
    
    data = []
    for image in images:
        temp = Image.open(image)
        temp = np.array(temp)
        temp = temp.reshape(28, 28, 1)
        temp = temp.astype('float32') / 255
        data.append(temp)
    
    with graph.as_default():
        prediction = model.predict([data])

    response = []
    for result in prediction:
        temp = {}
        for index, value in enumerate(result):
            temp[index] = str(value)
        response.append(temp)

    return flask.jsonify(response)

if __name__ == "__main__":
    load_model()
    app.run()